**💡목표 <br/>**
박스오피스 API를 이용하여 특정한 날짜 입력시 해당 박스오피스 순위가 나오는 검색기능을 구현한다.
<br/><br/>
**📈 예상효과 <br/>**
-ajax를 이용하여 외부데이터에 접근하는 방법을 배운다. <br/><br/>

**💭 아키텍처 구성 및 접근방법 계획**<br/>
입력창에 날짜입력 후 엔터키나 검색 버튼을 누르면 박스오피스가 보이는 프로젝트<br/>
![image](/uploads/5408a2ed47f0f90db8454e003942b62c/image.png) <br/> <br/>


1.AJAX를 이용하여 KOBIS 박스오피스 데이터를 가져온다. <br/>
2.KOBIS로부터 데이터가 호출되는 targetDt의 값부분을 사용자가 입력하는 값으로 바꾼다.<br/>
3.해당 API는 년도일의 정확한 날짜입력을 통해 데이터를 받아올 수 있다.<br/>
따라서 클라이언트가 입력창에 날짜 형식을 잘못넣었을 때는 에러 메세지를 표시하고 , 날짜 형식이 정확할 때만 데이터를 보여줄 수 있도록 한다.<br/>
4.박스오피스는 data.boxOfficeResult.dailyBoxOfficeList를 통해 가져올 수 있으며 , 리스트는 10개씩 저장되어있다.<br/>
5.10개를 모두 가져오기 위해 rank라는 변수에 저장하여 for문을 이용하여 개별적으로 원하는 데이터를 10개씩 가져온다.<br/>
6.클라이언트가 검색을 두번하는 경우를 대비하여,날짜 형식이 정확할 때는 자바스크립트의 html()함수를 이용하여 이전 데이터를 지우고 새로운 html을 이용하여 데이터를 보여준다. <br/>

**🚨 결론<br/>**
외부 json데이터는 바로 데이터를 보여주는 경우도 있지만 , key와 value를 자세히 봐야한다. <br/>
KOBIS의 데이터를 자세히 보면 , boxOfficeReult 라는 키(key)값의 안에는 'boxOfficeType'와 'dailyBoxOfficeList'라는 값이 둘다 공존한다. 그리고 boxOfficeType에도 value가 존재하며 dailyBoxOfficeList에도 value가 존재한다. <br/>
따라서 개별적인 값을 빼낼 때 , const rank = boxOfficeResult(첫번째 키).dailyBoxOfficeList(두번째 키);를 이용하여 배열값에 접근한 후 for문을 이용하여 rank[i].rank,rank[i].movieNm과 같이 원하는 데이터 정보를 빼와야한다. <br/>
![image](/uploads/7c22b598d39fe859067a499bef5ebc02/image.png) <br/>
<br/>
클라이언트는 입력창에 요청을 여러번 할 수 있다. 따라서 데이터의 값을 처음 빼서 줄 때 html()함수를 이용하여 html을 새로 만들어서 이전 값이 보이지 않게 전달해줘야한다. <br/>



**🔗참고자료<br/>**
http://kobis.or.kr/kobisopenapi/webservice/rest/boxoffice/searchDailyBoxOfficeList.json?key=f5eef3421c602c6cb7ea224104795888&targetDt=20230414
